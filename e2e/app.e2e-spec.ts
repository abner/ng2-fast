import { AppSerproPage } from './app.po';

describe('app-serpro App', function() {
  let page: AppSerproPage;

  beforeEach(() => {
    page = new AppSerproPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
