import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { InputContainerComponent } from './components/input/input-container.component';
import { FormComponent } from './components/form/form.component';
import { InputDirective } from './components/input/input.directive';
import { LabelDirective } from './components/input/label.directive';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    InputContainerComponent,
    FormComponent,
    InputDirective,
    LabelDirective
  ],
  exports: [
    InputContainerComponent,
    FormComponent,
    ReactiveFormsModule,
    InputDirective,
    LabelDirective
  ]
})
export class SharedModule { }
