

import { ElementRef, Directive, OnInit } from '@angular/core';

@Directive({
  selector: '[ng2fastMask]'
})
export class MaskDirective implements OnInit {



  constructor(private elementRef: ElementRef) {

  }

  ngOnInit() {
    (<any>jQuery(this.elementRef)).inputmask({
      mask: '9999'
    });
  }
}
