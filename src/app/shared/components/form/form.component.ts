import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'ng2fast-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  exportAs: 'fastForm'
})
export class FormComponent implements OnInit {

  @Input() title: string;
  @Input() confirmLabel: string;
  @Input() formGroup: FormGroup;

  @Output() formSubmited = new EventEmitter<any>();

  @ViewChild('form') form: NgForm;

  constructor() { }

  ngOnInit() {
  }

  formSubmit() {
    this.formSubmited.emit(this.form.value);
  }

  get status(): string {
    return this.formGroup.status;
  }
  get value(): any {
    return this.formGroup.value;
  }

  get valid(): boolean {
    return this.formGroup.valid;
  }

}
