import {  Directive, ElementRef, AfterViewInit } from '@angular/core';

@Directive({ selector: 'ng2fast-input-container label' })
export class LabelDirective implements AfterViewInit {

  innerHTML: string;

  constructor(private elementRef: ElementRef) {
    this.innerHTML = elementRef.nativeElement.innerHTML;
  }

  ngAfterViewInit() {
    console.log('LabelDirective', this);
  }
}

