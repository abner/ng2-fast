import { Directive, AfterViewInit } from '@angular/core';

@Directive({ selector: 'ng2fast-input-container input' })
export class InputDirective implements AfterViewInit {

  // @HostBinding('attr.class') cssClass: string = 'form-control';


  constructor() {
  }

  ngAfterViewInit() {
    console.log('Input Directive', this);
  }
}

