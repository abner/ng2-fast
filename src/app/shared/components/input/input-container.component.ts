import { Component, Input, OnInit, ContentChild, AfterContentInit, ViewChild } from '@angular/core';

import { FormControl, NgControl, FormGroup, AbstractControl } from '@angular/forms';
import { FormComponent } from './../form/form.component';

/**
 * Input Container is a container for inputs and will be used as a base for setup the
 * interaction with input fields
 *
 * @export
 * @class InputContainerComponent
 * @implements {OnInit}
 * @implements {AfterContentInit}
 */
@Component({
  selector: 'ng2fast-input-container',
  templateUrl: './input-container.component.html',
  styleUrls: ['./input-container.component.scss'],
  exportAs: 'ng2fastInput'
})
export class InputContainerComponent implements OnInit, AfterContentInit {

  /**
   * Label of the input
   * @type {string}
   * @memberOf InputContainerComponent
   */
  @Input() label: string;

  @Input() name: string;

  @Input() type: string;

  /**
   *
   *
   * @type {string}
   * @memberOf InputContainerComponent
   */
  @Input() inputId: string;
  /**
   *
   *
   * @type {*}
   * @memberOf InputContainerComponent
   */
  @Input() validationResult: any;

  /**
   *  Bind do Input passado para aplicar as validações e também operacionalizar
   *  mudanças nos valores e obter info do estado do campo
   *
   * @memberOf InputContainerComponent
   */

  /**
   *
   *
   * @type {FormControl}
   * @memberOf InputContainerComponent
   */

  // @ViewChild('inputControl') input: FormControl;

  // @ViewChild(ElementRef) html: ElementRef;

  // @Input() formGroup: FormGroup;
  /**
   * Creates an instance of InputContainerComponent.
   *
   *
   * @memberOf InputContainerComponent
   */
  constructor(
    private form: FormComponent
  ) { }

  get formGroup(): FormGroup {
    return this.form.formGroup;
  }

  get control():  AbstractControl {
    return this.form.formGroup.controls[this.name];
  }

  /**
   *
   *
   *
   * @memberOf InputContainerComponent
   */
  ngOnInit() {
  }

  /**
   *
   *
   * @readonly
   * @type {string}
   * @memberOf InputContainerComponent
   */
  get errorMessage(): string {
    return '';
  }

  /**
   *
   *
   *
   * @memberOf InputContainerComponent
   */
  ngAfterContentInit() {
    // (<HTMLInputElement>this.input.nativeElement).classList.add('form-control');
  }


}
