import { Component } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'ng2fast-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  fields = [
    {
      id: 'nome',
      label: 'Nome',
      name: 'nome',
      required: true,
      type: 'text'
    },
    {
      id: 'idade',
      label: 'idade',
      name: 'idade',
      required: false,
      type: 'number'
    }
  ];

  title = 'app works!';

  pessoa = {
    nome: 'ABNER'
  };

  formGroup: FormGroup;
  constructor(private fb: FormBuilder) {

    this.formGroup = this.buildFormGroup();


  }

  buildFormGroup(): FormGroup {
    let controls = {};
    this.fields.forEach((item) => {
      if (item.required) {
        controls[item.name] = [ '', Validators.required ];
      } else {
        controls[item.name] = [ '', null ];
      }
    });
    return this.fb.group(controls);
  }

  submitEventHandler(obj: any) {
    console.log('Form Submited', obj, this.formGroup.value);
  }

  get inputNome(): AbstractControl {
    return this.formGroup.get('nome');
  }
}
